#!/usr/bin/env python
# license removed for brevity
import rospy

# Include the ROS msg to be send
from geometry_msgs.msg import Point
from geometry_msgs.msg import PoseStamped
from rvo_path_planner.msg import CommandSequence

import numpy as np

class Commander(object):
    def __init__(self):
        self.veh_name = 'HL01'
        self.pub_command = rospy.Publisher('rvo_path_planner/command',CommandSequence, queue_size=1)
        self.sub_command = rospy.Subscriber("%s/pose"%(self.veh_name), PoseStamped, self.callback, queue_size=1)

    def callback(self,pose_msg):
        command_seq = CommandSequence()
        command_seq.veh_name = self.veh_name
        waypoints = list()
        waypoints.append(pose_msg.pose.position)
        command_seq.waypoints = waypoints
        self.pub_command.publish(command_seq)

if __name__ == '__main__':
    rospy.init_node('helmet_commander',anonymous=False)
    commander = Commander()
    rospy.spin()