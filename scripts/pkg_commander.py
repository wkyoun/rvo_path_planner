#!/usr/bin/env python
# license removed for brevity
import rospy
# Include the ROS msg to be send
from geometry_msgs.msg import Point
from geometry_msgs.msg import PoseStamped
from std_srvs.srv import Empty
# from quad_control.msg import Goal

from rvo_path_planner.msg import CommandSequence
from rvo_path_planner.msg import CommandComplete


import numpy as np
import random
import time

import udputil
import projutil

DEFAULT_HEIGHT = 1.0
BASE_LOCATION = [0,1.2]
PACKAGE_DICT = {
    'o': [1.25,-5.25],
    'p': [0.0,-5.25],
    't': [-1.25,-5.25]
}
PACKAGE_COLOR = {
    'o': [1.0, 0.4, 0.0],
    'p': [0.5, 0.0, 0.5],
    't': [0.0, 0.5, 0.5]
}

class Quad(object):    
    def __init__(self,name,host,port):
        self.name = name
        self.state = "INITIALIZING"
        self.print_state()
        self.udp_sender = udputil.UdpSender(host,port)
        self.pkgColor = [0.0,0.0,0.0,0.0]

    def send_pkg_udp(self):
        self.udp_sender.send('rvo_pkg_veh',self.pkgColor,time.time())

    def get_next_command(self,base_package = None):
        if base_package is not None:
            color = PACKAGE_COLOR[base_package]
            self.pkgColor[0] = color[0]
            self.pkgColor[1] = color[1]
            self.pkgColor[2] = color[2]
            self.pkgColor[3] = 1.0

        command = CommandSequence()
        command.veh_name = self.name
        command.header.stamp = rospy.Time.now()
        command.yaw = 0.0

        if self.state == "LAND_AT_BASE":
            self.state = "MOVE_TO_DELIVER"
            command.command = CommandSequence.MOVE
            if base_package is not None:
                pkg_destination = PACKAGE_DICT[base_package]
                command.waypoints.append(Point(pkg_destination[0],pkg_destination[1],DEFAULT_HEIGHT))
                # TODO send out udp
            else:
                rospy.loginfo("No package at base?")

        elif self.state == "RETURN_TO_BASE":
            self.state = "LAND_AT_BASE"
            command.command = CommandSequence.LOW
                         
        elif self.state == "MOVE_TO_DELIVER":
            self.state = "LAND_AT_DELIVER"
            command.command = CommandSequence.LOW

        elif self.state == "LAND_AT_DELIVER":
            self.state = "RETURN_TO_BASE"
            command.command = CommandSequence.MOVE
            command.waypoints.append(Point(BASE_LOCATION[0],BASE_LOCATION[1],DEFAULT_HEIGHT))
            self.pkgColor[3] = 0.0

        elif self.state == "INITIALIZING":
            self.state = "RETURN_TO_BASE"
            command.command = CommandSequence.MOVE
            command.waypoints.append(Point(BASE_LOCATION[0],BASE_LOCATION[1],DEFAULT_HEIGHT))
        else:
            print "self.state not handled."

        self.print_state()
        return command

    def print_state(self):
        rospy.loginfo("%s: %s" %(self.name,self.state))

class PkgCommander(object):
    def __init__(self,host,veh_name_list,port_list):
        self.host = host

        self.udp_base_sender = udputil.UdpSender(self.host,21599)
        self.udp_pkg_sender = udputil.UdpSender(self.host,21600)


        # Setup quads
        self.quads = dict()
        self.veh_name_list = veh_name_list
        for index,veh_name in enumerate(veh_name_list):
            self.quads[veh_name] = Quad(veh_name,self.host,port_list[index])    
        
        self.base_package = 'o'

        self.activated = False

        self.srv_activate = rospy.Service('~activate',Empty,self.cb_srv_activate)
        self.srv_deactivate = rospy.Service('~deactivate',Empty,self.cb_srv_deactivate)
        
        # Setup publisher and subscribers
        self.pub_command = rospy.Publisher('path_planner/command',CommandSequence, queue_size=10)
        self.sub_complete = rospy.Subscriber('path_planner/command_complete', CommandComplete, self.callback_complete)

        rospy.loginfo("Call /%s/activate service to activate pkg_commander"%(rospy.get_name()))

    def cb_srv_activate(self, req):
        self.activated = True
        rospy.loginfo("%s is activated."%(rospy.get_name()))
        return []

    def cb_srv_deactivate(self, req):
        rospy.loginfo("%s is deactivated."%(rospy.get_name()))
        self.activated = False
        return []

    # def get_random_point(self):
    #     return Point(np.random.uniform(-1.5,1.5,1)[0],np.random.uniform(-6,1.5,1)[0],1.0)
    def callback_complete(self, complete_msg):
        if self.activated:
            if ((complete_msg.veh_name in self.quads.keys()) and complete_msg.success):
                if self.quads[complete_msg.veh_name].state == "LAND_AT_BASE":
                    next_command = self.quads[complete_msg.veh_name].get_next_command(self.base_package)
                    # Generate a random package
                    self.base_package = np.random.choice(PACKAGE_DICT.keys(),1)[0]
                    # Send out
                    self.udp_pkg_sender.send('base_pkg',PACKAGE_COLOR[self.base_package],time.time())

                else:
                    next_command = self.quads[complete_msg.veh_name].get_next_command()

                self.pub_command.publish(next_command)

            else:
                time_on_task = rospy.Time.now() - complete_msg.issue_time
                # TODO: Handle timeouts

    def cb_udp_send(self,event):
        self.udp_base_sender.send('b1',np.array(BASE_LOCATION),time.time())

        for name,quad in self.quads.iteritems():
            quad.send_pkg_udp()

        # Send base information
        self.udp_base_sender.send('d1',np.array(PACKAGE_DICT['o']),time.time())
        self.udp_base_sender.send('d2',np.array(PACKAGE_DICT['p']),time.time())
        self.udp_base_sender.send('d3',np.array(PACKAGE_DICT['t']),time.time())

if __name__ == '__main__':
    rospy.init_node('pkg_commander',anonymous=False)

    host = rospy.get_param('~host')
    veh_list = rospy.get_param("~vehicle_list")
    port_list = rospy.get_param('~port_list')

    pkg_commander = PkgCommander(host,veh_list,port_list)
    pub_udp_timer = rospy.Timer(rospy.Duration.from_sec(1.0/50.0),pkg_commander.cb_udp_send)
    rospy.spin()
