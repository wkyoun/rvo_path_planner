##Dependencies##
TODO:Add dependencies
* `raven_rviz`
* `acl_msgs`
* `acl-ros-pkg`
* `acl-utils`

##How to use rvo_path_planner to fly (simulated) quads##

* Launch the visualizer
```
roslaunch rvo_path_planner raven_rviz.launch
```
* Launch simulated quads
```
roslaunch quad_control control.launch veh:=BQ num:=01 sim:=1 x:=0 y:=0
```
You can launch multiple ones with different numbers and initial positions. If you want to fly real quads, omit the `sim` parameter
* Launch the path planner
```
roslaunch rvo_path_planner path_planner.launch
```
This will start a `path_planner` node.
* Activate the path_planner
```
rosservice call /path_planner/activate
```
The path planner will start sending commands to the quads through the `BQ0#/goal` topics.

Starting from here, there are a couple of ways to fly the quads
### Through the command topic ###
You can send waypoints to quads by publishing `rvo_path_planner/CommandSequence` messages to the `/path_planner/command` topic. See the msg file for details.

### Through interactive markers in rviz ###
* Right click the quad in rviz and select `Take off`. This activates the interactive marker (instead of actually taking off).
* Drag the interactive marker to set waypoints for the quad.
* Use the up arrow of the interactive marker in rviz to lift the quad off the ground. Note that the command is sent when the interactive marker is released.
* Drag the box part of the interactive marker to send waypoints command. The path planner will plan a path when you release the marker and the quad will start following the path.
* Click the `land` checkbox in the right-click menu to snap the marker back to the quad and disable the marker. Dragging and releasing a disabled marker no longer sends a command to the path planner.

### Through service calls of the path planner ###
The `path_planner` node provides the following services
* `activate`: Start sending commands to the quads
* `deactivate`: Stop the path planner from sending any commands to the quads
* `take_off_all`: Send take off command to all quads
* `land_all`: Send land command to all quads
* `kill_all`: Send kill motor commands to all quads for emergency

## Parameters for the path planner ##
Path planner and quad specific parameters can be set in the `path_planner.launch` launch file.